package com.tcwgq.cas_mvc.controller;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tcwgq
 * @since 2022/6/27 17:22
 **/
@Controller
@RequestMapping("/sso")
public class SSOController {
    /**
     * 测试资源1
     */
    @GetMapping(value = "/page")
    public String page() {
        return "sso";
    }

    /**
     * 测试资源1
     */
    @ResponseBody
    @GetMapping(value = "/info")
    public String info(HttpServletRequest request) {
        String remoteUser = request.getRemoteUser();
        AttributePrincipal principal = (AttributePrincipal) request.getUserPrincipal();
        return "remoteUser=" + remoteUser + ",principalName=" + principal.getName() + ",principalAttributes=" + principal.getAttributes();
    }

}
