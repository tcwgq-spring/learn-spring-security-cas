<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.jasig.cas.client.authentication.AttributePrincipal" %>
<%
    String remoteUser = request.getRemoteUser();
    AttributePrincipal principal = (AttributePrincipal) request.getUserPrincipal();
    String attributes = principal.getAttributes().toString();
%>
<html>
<head>
    <title>sso测试</title>
</head>
<body>
remoteUser:<%=remoteUser%><br/>
principalName:<%=principal.getName()%><br/>
principalAttributes:<%=attributes%><br/>
<%--TODO 注意：这里的登出只有把同一浏览器中所有和当前会话关联的页面关闭后才能起作用--%>
<a href="http://127.0.0.1:8080/cas/logout?service=http://127.0.0.1:8082/index.jsp">logout</a>
</body>
</html>