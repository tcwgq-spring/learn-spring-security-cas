<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<h1>欢迎来到首页！</h1>
<a href="${pageContext.request.contextPath}/sso/page">sso page</a><br/>
<a href="${pageContext.request.contextPath}/sso/info">sso info</a><br/>
<%--TODO 注意：这里的登出只有把同一浏览器中所有和当前会话关联的页面关闭后才能起作用--%>
<a href="http://127.0.0.1:8080/cas/logout?service=http://127.0.0.1:8082/index.jsp">logout</a>
</body>
</html>