package com.tcwgq.securitycasspringbootb;

import com.tcwgq.securitycasspringbootb.config.CasProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(CasProperties.class)
public class SecurityCasSpringbootBApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCasSpringbootBApplication.class, args);
    }

}
