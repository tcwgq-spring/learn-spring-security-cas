package com.tcwgq.securitycasspringbootb.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * 此处可以真做权限的渲染，没必要在查一遍数据库
 */
public class UserDetailServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 校验用户是否是VIP
        // 用户相关操作行为
        // 从数据库查询出来的密码
        String password = "$2a$10$XbhYJ.YQq3Ns/n7JEqpMxuRPFs/Nbma.smfDANnyaM9JpUQOvGwtC";
        // 校验通过后放行
        // 添加授权信息
        List<GrantedAuthority> grantedList = new ArrayList<>();
        // 这里需要给用户授权对应的角色
        grantedList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        grantedList.add(new SimpleGrantedAuthority("ROLE_USER"));

        return new User(username, password, grantedList);
    }

}
