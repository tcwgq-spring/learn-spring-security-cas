package com.tcwgq.securitycasspringbootb.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "cas")
public class CasProperties {
    private String casServerUrl;

    private String casServerLoginUrl;

    private String casServerLogoutUrl;

    private String appServerUrl;

    private String appLoginUrl;

    private String appLogoutUrl;

}
