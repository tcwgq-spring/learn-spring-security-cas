package com.tcwgq.securitycasspringboota;

import com.tcwgq.securitycasspringboota.config.CasProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(CasProperties.class)
public class SecurityCasSpringbootAApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCasSpringbootAApplication.class, args);
    }

}
