package com.tcwgq.casspringbootb;

import org.jasig.cas.client.boot.configuration.EnableCasClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCasClient
@SpringBootApplication
public class CasSpringbootBApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasSpringbootBApplication.class, args);
    }

}
