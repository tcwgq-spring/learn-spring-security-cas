package com.tcwgq.casspringbootb.controller;

import lombok.extern.slf4j.Slf4j;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/user")
public class TestController {
    @RequestMapping("/info")
    public String info(HttpServletRequest request) {
        String remoteUser = request.getRemoteUser();
        AttributePrincipal principal = (AttributePrincipal) request.getUserPrincipal();
        return "remoteUser=" + remoteUser + ",principalName=" + principal.getName() + ",principalAttributes=" + principal.getAttributes();
    }

}