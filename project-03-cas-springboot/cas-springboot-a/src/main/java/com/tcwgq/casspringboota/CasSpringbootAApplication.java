package com.tcwgq.casspringboota;

import org.jasig.cas.client.boot.configuration.EnableCasClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCasClient
@SpringBootApplication
public class CasSpringbootAApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasSpringbootAApplication.class, args);
    }

}
