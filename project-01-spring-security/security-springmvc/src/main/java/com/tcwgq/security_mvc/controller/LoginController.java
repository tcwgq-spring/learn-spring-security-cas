package com.tcwgq.security_mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author tcwgq
 * @since 2022/6/27 17:22
 **/
@Controller
@RequestMapping("/login")
public class LoginController {
    @GetMapping("/to")
    public String to() {
        return "login";
    }

    /**
     * 自定义登陆接口
     */
    @ResponseBody
    @PostMapping(value = "/do")
    public String login(String username, String password) {
        System.out.println(username);
        System.out.println(password);
        return "success";
    }

    /**
     * POST方式支持 authentication-success-forward-url="/login/success"
     */
    @RequestMapping(value = "/success", method = {RequestMethod.GET, RequestMethod.POST})
    public String loginSuccess(Model model) {
        model.addAttribute("msg", "登陆成功!");
        return "login_success";
    }

    @RequestMapping(value = "/fail", method = {RequestMethod.GET, RequestMethod.POST})
    public String loginFail(Model model) {
        model.addAttribute("msg", "账号或者密码不正确!");
        return "login_fail";
    }

}
