package com.tcwgq.security_mvc_anno.init;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring Security初始化，这里有两种情况：
 * 1.若当前环境没有使用Spring或Spring MVC，则需要将 WebSecurityConfig(Spring Security配置类) 传入超
 * 类，以确保获取配置，并创建spring context。
 * <p>
 * 2.相反，若当前环境已经使用spring，我们应该在现有的springContext中注册Spring Security(上一步已经做将
 * WebSecurityConfig加载至rootcontext)，此方法可以什么都不做。
 * <p>
 * 在init包下定义SpringSecurityApplicationInitializer，继承AbstractSecurityWebApplicationInitializer
 */
public class SpringSecurityApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
    public SpringSecurityApplicationInitializer() {
        // super(WebSecurityConfig.class);
    }

}