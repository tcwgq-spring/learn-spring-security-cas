package com.tcwgq.security_mvc_anno.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * @author tcwgq
 * @since 2022/6/27 18:46
 **/
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 资源不拦截
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/css/**", "/fonts/**", "/images/**", "/js/**");
    }

    // 定义用户信息服务（查询用户信息）
    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        // 添加用户和权限
        manager.createUser(User.withUsername("root").password("root").authorities("p1").build());
        manager.createUser(User.withUsername("test").password("test").authorities("p2").build());
        return manager;
    }

    // 密码编码器
    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    // 安全拦截机制（最重要）
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                // 所有/r/**的请求必须认证通过
                .antMatchers("/r/**").authenticated()
                // 除了/r/**，其它的请求可以访问
                .anyRequest().permitAll()
                .and()
                // 允许表单登录
                .formLogin()
                // 指定了loginPage url，则表单提交的路径也是这个，spring security会自动生成一个post的同路径接口
                .loginPage("/login/to")
                .loginProcessingUrl("/login/do")
                // successForwardUrl和defaultSuccessUrl都不设置，登陆成功后默认跳到之前未登陆访问的路径
                // 自定义登录成功的页面地址
                .successForwardUrl("/login/success")
                // 登陆失败跳转页面
                .failureForwardUrl("/login/fail")
                // 登陆成功后的默认页面
                .defaultSuccessUrl("/admin/user/list", true)
        ;
    }

}
