package com.tcwgq.security_mvc_anno.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * 相当于applicationContext.xml
 *
 * @author tcwgq
 * @since 2022/6/27 16:21
 */
@Configuration
@ComponentScan(basePackages = "com.tcwgq.security_mvc_anno", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class)
})
public class ApplicationConfig {
    // 在此配置除了Controller的其它bean，比如：数据库链接池、事务管理器、业务bean等。
}
